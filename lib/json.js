const fs = require('fs')

/**
 * 
 * @param {string} path 
 * @returns Promise<any>
 */
module.exports.readJson = function(path) {
    return new Promise((resolve, reject) => {
        let string = fs.readFileSync(path).toString()
        try {
            resolve(JSON.parse(string))            
        } catch (error) {
            reject(error)
        }
    })
}